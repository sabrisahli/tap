package com.taptophone.telnet.taptophone.nfcreader;

import android.annotation.SuppressLint;
import android.util.Log;

import java.util.*;

@SuppressLint("UseSparseArrays")
public class CardAnalyst {


	// SELECT PPSE
	static private byte[] select_ppse = new byte[] { 0x00, (byte)0xA4, 0x04, 0x00, 0x0E, 0x32, 0x50, 0x41, 0x59, 0x2E, 0x53, 0x59, 0x53, 0x2E, 0x44, 0x44, 0x46, 0x30, 0x31, 0x00 } ;

	/* SELECT AID / select apdu for MAstercard , Visa and CB */
	static private byte[] select_aid_prefix  = new byte[] { 0x00, (byte)0xA4, 0x04 , 0x00 , 0x07 , 0x00  } ;

	// SELECT CB?
	static private byte[] select_cb  = new byte[] { 0x00, (byte)0xA4, 0x04 , 0x00 , 0x07 , (byte)0xA0 , 0x00 , 0x00 , 0x00 , 0x42 , 0x10 , 0x10, 0x00  } ;

	// SELECT MasterCard
	static private byte[] select_mastercard  = new byte[] { 0x00, (byte)0xA4, 0x04 , 0x00 , 0x07 , (byte)0xA0 , 0x00 , 0x00 , 0x00 , 0x04 , 0x10 , 0x10, 0x00  } ;
	// SELECT VISA
	static private byte[] select_visa  = new byte[] { 0x00, (byte)0xA4, 0x04 , 0x00 , 0x07 , (byte)0xA0 , 0x00 , 0x00 , 0x00 , 0x03 , 0x10 , 0x10, 0x00  } ;
	static private byte[][] select = new byte[][] { select_visa , select_mastercard , select_cb } ;

	static private String[] select_str = {"VISA", "MASTERCARD", "CB"};

	/* read record */
	static private byte readRecord[] = new byte[] { 0x00, (byte)0xB2, 0x00, (byte)0x00, 0x00 } ;
	/* get data */
	static private byte getData[] = new byte[] { (byte)0x80, (byte)0xCA, 0x00, (byte)0x00, 0x00 } ;


	public static HashMap<Integer,byte[]> readCard(ReaderCardInterface apdusender , HashMap<Integer,byte[]> card){
		String LOG_TAG="CardAnalyst";
		Log.i(LOG_TAG, "CardAnalyst.readCard");

		byte rep[] ;
		byte apdu[] = null;
		byte tag[] ;

		if(card == null){
			card = new HashMap<Integer,byte[]>() ;
			// Amount, Authorised (Numeric)
			card.put(0x9f02, new byte[] {0x00, 0x00, 0x00, 0x00, 0x01, 0x66 }) ;
			// Unpredictable Number
			card.put(0x9f37, new byte[] {0x12,0x34,0x56,0x78 }) ;
		}

		Log.i(LOG_TAG, "CardAnalyst.readCard: SELECT DF(PPSE) Cmd sending...");


		// SELECT PPSE
		rep = apdusender.sendApdu(select_ppse);

		System.out.println("\nrep :" + hextostring(rep)) ;

		if (rep == null)
			return card;

		Log.i(LOG_TAG, "CardAnalyst.readCard: SELECT DF(PPSE) Rsp = \n" + ProtFrame.getApdu(rep,0,card));
		//System.out.println(ProtFrame.getApdu(rep,0,card)) ;

		// AID (ADF name)
		byte aid[] = card.get(0x4f) ;

		if( aid != null){
			apdu = Arrays.copyOf(select_aid_prefix, aid.length+6) ;
			apdu[4] =  (byte) aid.length ;
			System.arraycopy(aid, 0, apdu, 5, aid.length) ;

		}else{
			for(int i = 0 ; i< select.length ; i++) {
				rep = apdusender.sendApdu(select[i]);
				Log.i(LOG_TAG, String.format("CardAnalyst.readCard: SELECT DF(%s) Cmd sending...", select_str[i]));
				//System.out.println("Send Apdu\napdu: "+hextostring(select[i])+"\nrep :" + hextostring(rep)) ;
				if( rep == null ){
					Log.i(LOG_TAG, "CardAnalyst.readCard: no application found 1");
					return card ;
				}
				if( rep[rep.length-2] == (byte)0x90 && rep[rep.length-1] == 0 ){
					Log.i(LOG_TAG, "CardAnalyst.readCard: application found");
					apdu = select[i] ;
				}
			}
		}

		if( apdu == null){
			Log.i(LOG_TAG, "CardAnalyst.readCard: no application found 2");
			return card;
		}


		//System.out.println("Send Apdu\napdu: "+hextostring(apdu));
		// SELECT AID
		Log.i(LOG_TAG, "CardAnalyst.readCard: SELECT AID Cmd sending...");
		rep = apdusender.sendApdu(apdu);

		if (rep == null)
			return card;
		Log.i(LOG_TAG, "CardAnalyst.readCard: SELECT AID Rsp = \n" + ProtFrame.getApdu(rep,0,card));

		//System.out.println("rep :" + hextostring(rep)) ;
		//System.out.println(ProtFrame.getApdu(rep,0,card)) ;

		// Processing Options Data Object List (PDOL)
		Log.i(LOG_TAG, "CardAnalyst.readCard: PDOL tag searching...");
		tag = card.get(0x9f38) ;
		apdu = ProtFrame.createGpo(tag , card) ;

		Log.i(LOG_TAG, "CardAnalyst.readCard: GET PROCESSING OPTIONS Cmd sending...");
		//System.out.println("Send Apdu\napdu: "+hextostring(apdu) ) ;
		rep = apdusender.sendApdu(apdu);

		if(rep == null || (rep.length == 2 && rep[0] != (byte)0x90) ){
			Log.e(LOG_TAG, "CardAnalyst.readCard: GET PROCESSING OPTIONS Rsp failure");
			//System.out.println("fail gpo !!") ;
			//System.out.println("rep :" + hextostring(rep)) ;
			return card;
		}

		Log.i(LOG_TAG, "CardAnalyst.readCard: GET PROCESSING OPTIONS Rsp = \n" + ProtFrame.getApdu(rep,0,card));

		//System.out.println("rep :" + hextostring(rep)) ;

		//System.out.println(ProtFrame.getApdu(rep,0,card)) ;

		// Response Message Template Format 2
		if(rep[0]== (byte)0x77){

			Log.i(LOG_TAG, "CardAnalyst.readCard: Response Message Template Format 2");
			// Application File Locator (AFL)
			tag = ProtFrame.searchTag(rep, 0x94) ;

		}else if(rep[0]== (byte)0x80){

			// Response Message Template Format 1
			Log.i(LOG_TAG, "CardAnalyst.readCard: Response Message Template Format 1");

			tag = new byte[(rep.length - 6)] ;
			System.arraycopy(rep, 4, tag, 0, (rep.length - 6));
		}

		if(tag != null && tag.length != 0){
			Log.i(LOG_TAG, "CardAnalyst.readCard: AFL found = " + Util.Bytes_to_Hex(tag));
			//System.out.println("AFL found : "+hextostring(tag)) ;
		}else{
			Log.e(LOG_TAG, "CardAnalyst.readCard: AFL not found");
			return card;
		}

		Vector<byte[]> files = getfile(tag) ;
		byte apdu2[] = readRecord.clone() ;
		byte t[] = new byte[2] ;

		for(Iterator<byte[]> i = files.iterator(); i.hasNext() ;  ){
			t = i.next() ;
			System.out.println("file : "+hextostring(t)) ;

			apdu2[2] = t[0] ;
			apdu2[3] = t[1] ;

			rep = apdusender.sendApdu(apdu2);
			//System.out.println("Send Apdu\napdu: "+hextostring(apdu2)+"\nrep :" + hextostring(rep)) ;

			Log.i(LOG_TAG, "CardAnalyst.readCard: GET PROCESSING OPTIONS Rsp = \n" + ProtFrame.getApdu(rep,0,card));
		}

		/* get historic transaction */
		// Log Entry
		Log.i(LOG_TAG, "CardAnalyst.readCard: Log Entry tag searching...");

		byte logEntry[] = card.get(0x9f4D) ;
		if(logEntry != null){
			apdu = readRecord.clone() ;

			int logEntryVal = logEntry[0] * 8 + 4;
			String argStr = String.format(Locale.ENGLISH, "logEntry:%d)", logEntryVal);

			apdu[2] = 1 ;
			apdu[3] = (byte) (logEntryVal) ;

			Log.i(LOG_TAG, String.format("CardAnalyst.readCard: READ RECORD(%s) Cmd sending...", argStr));

			//System.out.println("Send Apdu\napdu: "+hextostring(apdu)) ;
			rep = apdusender.sendApdu(apdu);
			//System.out.println("rep :" + hextostring(rep)) ;

			/* get historic transaction format*/
			// Log Format
			apdu = getData.clone() ;
			apdu[2] = (byte)0x9f ;
			apdu[3] = 0x4F ;
			Log.i(LOG_TAG, "CardAnalyst.readCard: GET DATA Cmd sending...");
			//System.out.println("Send Apdu\napdu: "+hextostring(apdu)) ;
			rep = apdusender.sendApdu(apdu);
			//System.out.println("rep :" + hextostring(rep)) ;
		}

		// send genetare AC.
		// Card Risk Management Data Object List 1 (CDOL1)
		Log.i(LOG_TAG, "CardAnalyst.readCard: CDOL1 tag searching...");

		tag = card.get(0x8c) ;

		if(tag == null){
			Log.e(LOG_TAG, "CardAnalyst.readCard: CDOL1 not found");
			return card ;
		}

		apdu = ProtFrame.createGAC(tag,card) ;
		Log.i(LOG_TAG, "CardAnalyst.readCard: GENERATE APPLICATION CRYPTOGRAM sending...");

		//System.out.println("Send Apdu\napdu: "+hextostring(apdu)) ;
		rep = apdusender.sendApdu(apdu);

		if(rep == null || (rep.length == 2 && rep[0] != (byte)0x90) ){
			//System.out.println("fail gac !!") ;
			//System.out.println("rep :" + hextostring(rep)) ;
			Log.e(LOG_TAG, "CardAnalyst.readCard: GENERATE APPLICATION CRYPTOGRAM Rsp failure");
			return card;
		}

		//System.out.println("rep :" + hextostring(rep)) ;
		Log.i(LOG_TAG, "CardAnalyst.readCard: GENERATE APPLICATION CRYPTOGRAM Rsp = \n" + ProtFrame.getApdu(rep,0,card));
		//System.out.println(ProtFrame.getApdu(rep,0,card)) ;

		return card ;

	}

	public static String hextostring(byte[] tab){
		String s = "" ;

		if(tab == null) {return s ; }

		for(int i=0 ; i<tab.length ;i++){
			s += (i > 0 ? " " : "") + String.format("%02X", (tab[i] & 0xFF));
		}

		return s ;
	}

	private static Vector<byte[]> getfile(byte afl[]){
		Vector<byte[]> files = new Vector<byte[]>();

		for(int i=0 ; 4*i<afl.length ; i++){
			byte a,b,c ;
			a = afl[4*i] ;
			b = afl[4*i+1] ;
			c = afl[4*i+2] ;

			for(byte j=b; j<=c ; j++ ){
				byte n[]= new byte[2] ;
				n[0] = j ;
				n[1] = (byte) (a+4);
				files.add(n) ;
			}
		}

		return files ;
	}
}
package com.taptophone.telnet.taptophone

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkInfo
import android.net.wifi.WifiManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.IsoDep
import android.os.AsyncTask
import android.support.v7.app.AlertDialog
import com.beust.klaxon.JsonObject
import com.beust.klaxon.JsonReader
import com.beust.klaxon.Klaxon
import com.taptophone.telnet.taptophone.nfcreader.CardAnalyst
import com.taptophone.telnet.taptophone.nfcreader.ReaderNfc
import kotlinx.android.synthetic.main.activity_main.*
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.security.KeyStore
import java.security.cert.CertificateFactory
import java.security.cert.X509Certificate
import java.text.SimpleDateFormat
import java.util.*
import javax.net.ssl.*


class KernelActivity : AppCompatActivity(), NfcAdapter.ReaderCallback {

    private val LOG_TAG: String = "KernelActivity"

    private var nfcAdapter: NfcAdapter? = null

    private var lastPan = ""
    private var sFormattedDate = "0000"



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        nfcAdapter = NfcAdapter.getDefaultAdapter(this)


    }

    public override fun onResume() {
        super.onResume()


        nfcAdapter?.enableReaderMode(
            this, this,
            NfcAdapter.FLAG_READER_NFC_A or NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK,
            null
        )
    }

    public override fun onPause() {
        super.onPause()
        nfcAdapter?.disableReaderMode(this)
    }


    override fun onTagDiscovered(tag: Tag?) {

        Utils.beep(20)

        val isoDep = IsoDep.get(tag)

        val sender = ReaderNfc(isoDep)
        val card1 = HashMap<Int, ByteArray>()

        card1[0x9f02] = byteArrayOf(0x00, 0x00, 0x00, 0x00, 0x00, 0x00)
        card1[0x9f37] = byteArrayOf(0x12, 0x34, 0x56, 0x78)

        val card = CardAnalyst.readCard(sender, card1)
        // Read PAN from card
        val bpan = card[0x5A]
        var pan = ""
        if (bpan != null) {
            pan = CardAnalyst.hextostring(bpan)
            Log.d("mypan ", pan)
            lastPan = pan.replace(" ", "").substring(0, 16)
        } else {
            var biso2: ByteArray? = card[0x57] // Pan from ISO 2
            if (biso2 != null) {
                biso2 = Arrays.copyOfRange(biso2, 0, 8)
                pan = CardAnalyst.hextostring(biso2)
                Log.d("mypan ", pan)
                lastPan = pan.replace(" ", "").substring(0, 16)
            }
        }
        // read Expiration Date from card
        val bdatexp = card[0x5F24]
        Log.d("bdatexp ", bdatexp.toString())

        if (bdatexp != null) {
            Log.d("bdatexp ", CardAnalyst.hextostring(bdatexp))

            var dateexp = CardAnalyst.hextostring(bdatexp).replace(" ", "/").substring(0, 8)
            Log.d("dateexp tag", dateexp)

            try {
                val date = SimpleDateFormat("yy/MM/dd").parse(dateexp)
                sFormattedDate = SimpleDateFormat("MMyy").format(date)
            } catch (E: Exception) {
                Log.w(LOG_TAG, "format DATE exception =" + E.message)
            }

            // read Expiration Date for VISA card
        } else {
            try {
                val tagvisa = card[0x57]
                var tagvisahex = CardAnalyst.hextostring(tagvisa)
                Log.d("tagvisa ", tagvisahex)
                var datevisa = tagvisahex.substring(tagvisahex.indexOf("D") + 1, tagvisahex.indexOf("D") + 7)
                Log.d("datevisa ", datevisa)
                datevisa = datevisa.replace(" ", "").substring(0, datevisa.length - 2)
                Log.d("datevisa2 ", datevisa)
                sFormattedDate = datevisa.substring(2, 4) + datevisa.substring(0, 2)
                Log.d("sFormattedDate ", sFormattedDate)

            } catch (E: Exception) {
                Log.w(LOG_TAG, "format VISA DATE exception =" + E.message)
            }


        }


        Log.d("lastPan ", lastPan)
        Log.d("sFormattedDate ", sFormattedDate)

        runOnUiThread {

            cardData.append("\n")
            cardData.append("\nPAN : " + lastPan)
            cardData.append("\nExpiration Date : " + sFormattedDate)


        }

        val JSONrequest=
            """{  "terminalIdentificationCode": " 01020058","messageType": "1206","serialNumberRP": "18222RP10615000","processingCode": "000000", "amountTransaction": "000000000960","systemTraceAuditNumber": "000002",
            "dateEffective": "1809","dateExpiration": "2010","pointOfServiceDataCode": "L1010151134C","cardSequenceNumber": "001", "functionCode": "200", "messageReasonCode": "1503", "cardAcceptorBusinessCode" : "",
            "acquiringInstitutionIdentificationCode" : "88108000101", "currencyCodeTransaction": "978" ,"encryptedPinBlock": "","securityInfoKsnForPinblock": "","iccData": "9F02060000000009609F03060000000000009F2608ED3E9D43423D0F154F07A000000042101082023D009F360200499F0702FF009F2701809F100706840A03A40002950500800080009F3704095C88EC9A031811129F3303E0F8
            C89F34034403029F3501219F4005FF00F0A0019B02E8009F660200009F7804000000009C01005F2A0209789F1A0202509F0A080001050100000000","encryptedCardholderDataEndToEnd" : "97AF5F831CE4698A51C55942B643
            17EF72AEC1BE059565A607475E66E1E363AB", "securityInfoKsnForCardholder": "FFFF01040000178000FE", "freeAdditionalApplicationData": "000000", "serviceID": "0002",
            "checksum": "0B7414571A7705D9C20FD3E8CDBB772A" }"""




        val objectString =
            """{
     "name" : "Joe",
     "age" : 23,
     "flag" : true

}"""

        JsonReader(StringReader(objectString)).use { reader -> reader.beginObject() {
                var name: String? = null
                var age: Int? = null
                var flag: Boolean? = null

                while (reader.hasNext()) {
                    val readName = reader.nextName()
                    when (readName) {
                        "name" -> name = reader.nextString()

                        "age" -> age = reader.nextInt()
                        "flag" -> flag = reader.nextBoolean()



                        else ->

                           //Assert.fail("Unexpected name: $readName")
                            Log.e(LOG_TAG,"Unexpected name: $readName")
                    }
                }

                Log.d(LOG_TAG,"Unexpected name: $name")
                Log.d(LOG_TAG,"Unexpected name: $age")
                Log.d(LOG_TAG,"Unexpected name: $flag")

            }
        }



        val myTaskParams = ArrayList<String>()
        myTaskParams.add(0, lastPan)
        myTaskParams.add(1, sFormattedDate)

        var doTransaction=true

        if (doTransaction) {

            AsyncTaskExample().execute(myTaskParams)
            Log.d("startPayment:ogone", "Ogone ")
        } else {


         /*   this@KernelActivity.runOnUiThread(java.lang.Runnable {

                AlertDialog.Builder(this).setTitle("No Internet Connection")
                    .setMessage("Please check your internet connection and try again")
                    .setPositiveButton("ok") { _, _ -> }
                    .setIcon(android.R.drawable.ic_dialog_alert).show()

                val wifiManager = applicationContext.getSystemService(Context.WIFI_SERVICE) as WifiManager
                wifiManager.isWifiEnabled = true


            })*/

        }

    }


    inner class AsyncTaskExample : AsyncTask<ArrayList<String>, String, ArrayList<String>>() {


        override fun onPreExecute() {
            super.onPreExecute()


            this@KernelActivity.runOnUiThread(java.lang.Runnable {

                //  progressBar2.visibility = View.VISIBLE


            })


        }


        override fun doInBackground(vararg p0: ArrayList<String>): ArrayList<String> {

            val myTaskParams = ArrayList<String>()
            val JSONrequest=
                """{  "terminalIdentificationCode": " 01020058","messageType": "1206","serialNumberRP": "18222RP10615000","processingCode": "000000", "amountTransaction": "000000000960","systemTraceAuditNumber": "000002",
            "dateEffective": "1809","dateExpiration": "2010","pointOfServiceDataCode": "L1010151134C","cardSequenceNumber": "001", "functionCode": "200", "messageReasonCode": "1503", "cardAcceptorBusinessCode" : "",
            "acquiringInstitutionIdentificationCode" : "88108000101", "currencyCodeTransaction": "978" ,"encryptedPinBlock": "","securityInfoKsnForPinblock": "","iccData": "9F02060000000009609F03060000000000009F2608ED3E9D43423D0F154F07A000000042101082023D009F360200499F0702FF009F2701809F100706840A03A40002950500800080009F3704095C88EC9A031811129F3303E0F8
            C89F34034403029F3501219F4005FF00F0A0019B02E8009F660200009F7804000000009C01005F2A0209789F1A0202509F0A080001050100000000","encryptedCardholderDataEndToEnd" : "97AF5F831CE4698A51C55942B643
            17EF72AEC1BE059565A607475E66E1E363AB", "securityInfoKsnForCardholder": "FFFF01040000178000FE", "freeAdditionalApplicationData": "000000", "serviceID": "0002",
            "checksum": "0B7414571A7705D9C20FD3E8CDBB772A" }"""

            var myBaseurl = "https://89.96.250.46:19519/c-1-cb2like/"
            var Result: String = "";
            var urlToSend = myBaseurl+ JSONrequest


            Log.d(LOG_TAG, "API_URL " + urlToSend)
//**/
            // Load CAs from an InputStream
// (could be from a resource or ByteArrayInputStream or ...)
            val cf: CertificateFactory = CertificateFactory.getInstance("X.509")
// From https://www.washington.edu/itconnect/security/ca/load-der.crt
            val caInput: InputStream = BufferedInputStream(FileInputStream("load-der.crt"))
            val ca: X509Certificate = caInput.use {
                cf.generateCertificate(it) as X509Certificate
            }
            System.out.println("ca=" + ca.subjectDN)

// Create a KeyStore containing our trusted CAs
            val keyStoreType = KeyStore.getDefaultType()
            val keyStore = KeyStore.getInstance(keyStoreType).apply {
                load(null, null)
                setCertificateEntry("ca", ca)
            }

// Create a TrustManager that trusts the CAs inputStream our KeyStore
            val tmfAlgorithm: String = TrustManagerFactory.getDefaultAlgorithm()
            val tmf: TrustManagerFactory = TrustManagerFactory.getInstance(tmfAlgorithm).apply {
                init(keyStore)
            }

// Create an SSLContext that uses our TrustManager
            val context: SSLContext = SSLContext.getInstance("TLS").apply {
                init(null, tmf.trustManagers, null)
            }

// Tell the URLConnection to use a SocketFactory from our SSLContext
            val url = URL("https://certs.cac.washington.edu/CAtest/")
            val urlConnection = url.openConnection() as HttpsURLConnection
            urlConnection.sslSocketFactory = context.socketFactory

            val inputStream: InputStream = urlConnection.inputStream
            Log.d(LOG_TAG, "inputStream" + inputStream)
          //  IOUtils.copy(inputStream, System.out)
            //**/


            try {

                val URL = URL(urlToSend)
                val connect = URL.openConnection() as HttpsURLConnection

                connect.readTimeout = 8000
                connect.connectTimeout = 8000
                connect.requestMethod = "POST"
                connect.doOutput = true
                connect.connect()

                val ResponseCode: Int = connect.responseCode

                Log.d(LOG_TAG, "ResponseCode" + ResponseCode)
                myTaskParams.add(0, ResponseCode.toString())
                if (ResponseCode == 200) {

                    val tempStream: InputStream = connect.inputStream;
                    Result = ConvertToString(tempStream)
                }

                myTaskParams.add(1, Result)


            } catch (Ex: Exception) {

                Log.d("", "Error in doInBackground " + Ex.message)

            }


            return myTaskParams
        }


        override fun onPostExecute(result: ArrayList<String>) {
            super.onPostExecute(result)

            Log.d(LOG_TAG, "result" + result)
        }

    }


        fun ConvertToString(inStream: InputStream): String {

            var Result: String = ""

            val isReader = InputStreamReader(inStream)
            var bReader = BufferedReader(isReader)
            var temp_str: String?

            try {

                while (true) {

                    temp_str = bReader.readLine()

                    if (temp_str == null) {

                        break
                    }
                    Result += temp_str;
                }


            } catch (Ex: Exception) {
                Log.e(LOG_TAG, "Error in ConvertToString " + Ex.printStackTrace())
            }

            return Result

        }

        fun isNetworkAvailable(): Boolean {
            val connectivityManager = getSystemService(Context.CONNECTIVITY_SERVICE)
            return if (connectivityManager is ConnectivityManager) {
                val networkInfo: NetworkInfo? = connectivityManager.activeNetworkInfo
                networkInfo?.isConnected ?: false
            } else false
        }


    }
